namespace perlin_simple.Shared
{
    public class PerlinNoise
    {
        // 2D noise variables
        public int OutputWidth = 128;
        public int OutputHeight = 128;
        public List<float> NoiseSeed2D = new();
        public List<float> PerlinNoise2D = new();

        // 1D noise variables
        public List<float> NoiseSeed1D = new();
        public List<float> PerlinNoise1D = new();
        public int OutputSize = 128;

        public int OctaveCount = 8;
        public float ScalingBias = 1.0f;

        public PerlinNoise()
        {
            NewSeed();
        }

        public void NewSeed()
        {
            Random random = new Random();
            NoiseSeed2D.Clear();
            NoiseSeed1D.Clear();
            for (int i = 0; i < OutputSize; i++)
            {
                NoiseSeed1D.Add((float)random.NextDouble());
            }
            for (int i = 0; i < OutputWidth * OutputHeight; i++)
            {
                NoiseSeed2D.Add((float)random.NextDouble());
            }
            PerlinNoise1D = GenPerlinNoise1D(OutputSize, NoiseSeed1D, OctaveCount, ScalingBias);
            PerlinNoise2D = GenPerlinNoise2D(OutputWidth, OutputHeight, NoiseSeed2D, OctaveCount, ScalingBias);
        }

        public static List<float> GenPerlinNoise1D(int cnt, List<float> seed, int octaves, float bias)
        {
            List<float> output = new();
            for (int i = 0; i < cnt; i++)
            {
                float noise = 0.0f;
                float scaleAcc = 0.0f;
                float scale = 1.0f;
                for (int o = 0; o < octaves; o++)
                {
                    int pitch = cnt >> o;
                    int dot1 = (i / pitch) * pitch;
                    int dot2 = (dot1 + pitch) % cnt;
                    float blend = (i - dot1) / (float)pitch;
                    float sample = (1.0f - blend) * seed[dot1] + blend * seed[dot2];
                    scaleAcc += scale;
                    noise += sample * scale;
                    scale /= bias;
                }
                output.Add(noise / scaleAcc);
            }
            return output;
        }
        public static List<float> GenPerlinNoise2D(int width, int height, List<float> seed, int octaves, float bias)
        {
            List<float> output = new();
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    float noise = 0.0f;
                    float scaleAcc = 0.0f;
                    float scale = 1.0f;
                    for (int o = 0; o < octaves; o++)
                    {
                        int pitch = width >> o;
                        int dotX1 = (x / pitch) * pitch;
                        int dotY1 = (y / pitch) * pitch;

                        int dotX2 = (dotX1 + pitch) % width;
                        int dotY2 = (dotY1 + pitch) % width;

                        float blendX = (x - dotX1) / (float)pitch;
                        float blendY = (y - dotY1) / (float)pitch;

                        float sampleT = (1.0f - blendX) * seed[dotY1 * width + dotX1] + blendX * seed[dotY1 * width + dotX2];
                        float sampleB = (1.0f - blendX) * seed[dotY2 * width + dotX1] + blendX * seed[dotY2 * width + dotX2];

                        scaleAcc += scale;
                        noise += (blendY * (sampleB - sampleT) + sampleT) * scale;
                        scale /= bias;
                    }
                    output.Add(noise / scaleAcc);
                }
            }
            return output;
        }
    }
}
