# perlin-simple-noise

## Description:
This project offers a simple implementation of the Perlin noise generator in both 1D and 2D formats. Perlin noise is a type of gradient noise, commonly used in computer graphics for procedural texture generation, terrain formation, and other visual effects. The noise produced is deterministic, meaning the same input parameters will always produce the same output.

## Features:
Generate Perlin noise in 1D and 2D.
Customizable parameters such as octave count and scaling bias for fine-tuning the noise details.
Interactive visual interface to display 2D Perlin noise with real-time parameter adjustments.
Gradient color mapping for visual representation of the generated noise.

## How to Use:
1. Adjust the desired octave count and scaling bias using the controls.
2. Click on "Generate New Noise" to generate and display the new noise pattern.
3. The generated Perlin noise will be displayed below the controls as a grid of colored cells, providing a visual representation of the noise values.

## Limitations:
The current implementation is designed for demonstration purposes. For larger-scale applications or more features, further optimization and enhancements might be required.

## Conclusion:
This Perlin Noise Generator project serves as a basic introduction to Perlin noise and its implementation in computer graphics (or other pseudo random generations). By adjusting parameters, users can explore the nature of Perlin noise and how it can be used in various applications.